//
//  MobileDataUsagesResponseEntity+Mapping.swift
//  MobileDataUsage
//
//  Created by Anand, Chetan on 16/4/20.
//  Copyright © 2020 sphtech. All rights reserved.
//

import CoreData
import Foundation

extension MobileDataUsageResponseEntity {
    func toDTO() -> MobileDataUsageResponseDTO {
        var result = MobileDataUsageResultDTO()
        result.records = record?.allObjects.map { ($0 as! MobileDataUsageRecordEntity).toDTO() }
        return .init(help: nil, success: true, result: result)
    }
}

extension MobileDataUsageRecordEntity {
    func toDTO() -> MobileDataUsageRecordsDTO {
        return MobileDataUsageRecordsDTO(volume_of_mobile_data: volume, quarter: quarter, _id: Int(id))
    }
}

extension MobileDataUsageRequestDTO {
    func toEntity(in context: NSManagedObjectContext) -> MobileDataUsageRequestEntity {
        let entity: MobileDataUsageRequestEntity = .init(context: context)
        entity.limit = Int32(limit)
        entity.resourceId = resourceId
        return entity
    }
}

//
extension MobileDataUsageResponseDTO {
    func toEntity(in context: NSManagedObjectContext) -> MobileDataUsageResponseEntity {
        let entity: MobileDataUsageResponseEntity = .init(context: context)
        result?.records?.forEach { records in
            entity.addToRecord(records.toEntity(in: context))
        }
        return entity
    }
}

extension MobileDataUsageRecordsDTO {
    func toEntity(in context: NSManagedObjectContext) -> MobileDataUsageRecordEntity {
        let entity: MobileDataUsageRecordEntity = .init(context: context)
        entity.volume = volume_of_mobile_data
        entity.quarter = quarter
        entity.id = Int64(_id ?? 0)
        return entity
    }
}
