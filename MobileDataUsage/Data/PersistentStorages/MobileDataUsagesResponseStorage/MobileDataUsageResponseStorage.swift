//
//  MobileDataUsageResponseStorage.swift
//  MobileDataUsage
//
//  Created by Anand, Chetan on 16/4/20.
//  Copyright © 2020 sphtech. All rights reserved.
//

import Foundation

protocol MobileDataUsageResponseStorage {
    func getResponse(for request: MobileDataUsageRequestDTO, completion: @escaping (Result<MobileDataUsageResponseDTO?, CoreDataStorageError>) -> Void)
    func save(response: MobileDataUsageResponseDTO, for requestDto: MobileDataUsageRequestDTO)
}
