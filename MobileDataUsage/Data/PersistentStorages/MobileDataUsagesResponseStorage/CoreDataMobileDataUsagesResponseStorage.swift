//
//  CoreDataMobileDataUsagesResponseStorage.swift
//  MobileDataUsage
//
//  Created by Anand, Chetan on 16/4/20.
//  Copyright © 2020 sphtech. All rights reserved.
//

import CoreData
import Foundation

final class CoreDataMobileDataUsagesResponseStorage {
    private let coreDataStorage: CoreDataStorage

    init(coreDataStorage: CoreDataStorage = CoreDataStorage.shared) {
        self.coreDataStorage = coreDataStorage
    }

    // MARK: - Private

    private func fetchRequest(for requestDto: MobileDataUsageRequestDTO) -> NSFetchRequest<MobileDataUsageRequestEntity> {
        let request: NSFetchRequest = MobileDataUsageRequestEntity.fetchRequest()
        request.predicate = NSPredicate(format: "resourceId = %@ AND limit = %d", requestDto.resourceId, requestDto.limit)
        return request
    }

    private func deleteResponse(for requestDto: MobileDataUsageRequestDTO, in context: NSManagedObjectContext) {
        let request = fetchRequest(for: requestDto)

        do {
            if let result = try context.fetch(request).first {
                context.delete(result)
            }
        } catch {
            print(error)
        }
    }
}

extension CoreDataMobileDataUsagesResponseStorage: MobileDataUsageResponseStorage {
    func getResponse(for requestDto: MobileDataUsageRequestDTO, completion: @escaping (Result<MobileDataUsageResponseDTO?, CoreDataStorageError>) -> Void) {
        coreDataStorage.performBackgroundTask { context in
            do {
                let fetchRequest = self.fetchRequest(for: requestDto)
                let requestEntity = try context.fetch(fetchRequest).first
                let responseDto = requestEntity?.response?.toDTO()

                completion(.success(responseDto))
            } catch {
                completion(.failure(CoreDataStorageError.readError(error)))
                print(error)
            }
        }
    }

    func save(response responseDto: MobileDataUsageResponseDTO, for requestDto: MobileDataUsageRequestDTO) {
        coreDataStorage.performBackgroundTask { context in
            do {
                self.deleteResponse(for: requestDto, in: context)

                let requestEntity = requestDto.toEntity(in: context)
                requestEntity.response = responseDto.toEntity(in: context)

                try context.save()
            } catch {
                print(error)
            }
        }
    }
}
