//
//  DefaultMobileDataUsageRepository.swift
//  mvvm-clean-architecture
//
//  Created by Anand, Chetan on 12/4/20.
//  Copyright © 2020 Anand, Chetan. All rights reserved.
//

import Foundation

final class DefaultMobileDataUsageRepository {
    private let dataTransferService: DataTransferService
    private let cache: MobileDataUsageResponseStorage

    init(dataTransferService: DataTransferService, cache: MobileDataUsageResponseStorage) {
        self.dataTransferService = dataTransferService
        self.cache = cache
    }
}

extension DefaultMobileDataUsageRepository: MobileDataUsageRepository {
    func fetchMobileDataUsageList(requestValue: DisplayMobileDataUsageUseCaseRequestValue, cached: @escaping (MobileDataUsagePage) -> Void, completion: @escaping (Result<MobileDataUsagePage, Error>) -> Void) -> Cancellable? {
        let requestDTO = MobileDataUsageRequestDTO(limit: requestValue.limit, resourceId: requestValue.resourceId)
        let task = RepositoryTask()

        cache.getResponse(for: requestDTO) { result in
            if case let .success(responseDTO?) = result {
                cached(responseDTO.mapToDomain())
            }

            guard !task.isCancelled else { return }

            let endpoint = APIEndpoints.getMobileDataUsage(with: requestDTO)
            task.networkTask = self.dataTransferService.request(with: endpoint) { result in
                switch result {
                case let .success(responseDTO):
                    self.cache.save(response: responseDTO, for: requestDTO)
                    completion(.success(responseDTO.mapToDomain()))
                case let .failure(error):
                    completion(.failure(error))
                }
            }
        }
        return task
    }
}
