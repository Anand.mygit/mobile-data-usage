//
//  APIEndpoints.swift
//  mvvm-clean-architecture
//
//  Created by Anand, Chetan on 12/4/20.
//  Copyright © 2020 Anand, Chetan. All rights reserved.
//

import Foundation

struct APIEndpoints {
    static func getMobileDataUsage(with MobileDataUsageRequestDTO: MobileDataUsageRequestDTO) -> Endpoint<MobileDataUsageResponseDTO> {
        return Endpoint(path: "datastore_search",
                        method: .get,
                        queryParametersEncodable: MobileDataUsageRequestDTO)
    }
}
