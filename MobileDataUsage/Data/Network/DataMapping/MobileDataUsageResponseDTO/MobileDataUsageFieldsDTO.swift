//
//  MobileDataUsageResponseDTO+Mapping.swift
//  mvvm-clean-architecture
//
//  Created by Anand, Chetan on 12/4/20.
//  Copyright © 2020 Anand, Chetan. All rights reserved.
//

import Foundation
struct MobileDataUsageFieldsDTO: Codable {
    let type: String?
    let id: String?

    enum CodingKeys: String, CodingKey {
        case type
        case id
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        type = try values.decodeIfPresent(String.self, forKey: .type)
        id = try values.decodeIfPresent(String.self, forKey: .id)
    }
}
