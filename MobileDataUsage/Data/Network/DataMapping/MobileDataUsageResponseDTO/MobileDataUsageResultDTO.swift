//
//  MobileDataUsageResponseDTO+Mapping.swift
//  mvvm-clean-architecture
//
//  Created by Anand, Chetan on 12/4/20.
//  Copyright © 2020 Anand, Chetan. All rights reserved.
//

import Foundation
struct MobileDataUsageResultDTO: Codable {
    var resource_id: String?
    var fields: [MobileDataUsageFieldsDTO]?
    var records: [MobileDataUsageRecordsDTO]?
    var _links: MobileDataUsageLinksDTO?
    var limit: Int?
    var total: Int?

    init() {}
    enum CodingKeys: String, CodingKey {
        case resource_id
        case fields
        case records
        case _links
        case limit
        case total
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        resource_id = try values.decodeIfPresent(String.self, forKey: .resource_id)
        fields = try values.decodeIfPresent([MobileDataUsageFieldsDTO].self, forKey: .fields)
        records = try values.decodeIfPresent([MobileDataUsageRecordsDTO].self, forKey: .records)
        _links = try values.decodeIfPresent(MobileDataUsageLinksDTO.self, forKey: ._links)
        limit = try values.decodeIfPresent(Int.self, forKey: .limit)
        total = try values.decodeIfPresent(Int.self, forKey: .total)
    }
}
