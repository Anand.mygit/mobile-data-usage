//
//  MobileDataUsageResponseDTO+Mapping.swift
//  mvvm-clean-architecture
//
//  Created by Anand, Chetan on 12/4/20.
//  Copyright © 2020 Anand, Chetan. All rights reserved.
//

import Foundation
struct MobileDataUsageRecordsDTO: Codable {
    let volume_of_mobile_data: String?
    let quarter: String?
    let _id: Int?

    enum CodingKeys: String, CodingKey {
        case volume_of_mobile_data
        case quarter
        case _id
    }
}

extension MobileDataUsageRecordsDTO {
    func mapToDomain() -> MobileDataUsageRecord {
        let volume = Double(volume_of_mobile_data ?? "0.0")
        return .init(id: String(describing: _id ?? Int(0.0)), volume: volume, quarter: quarter)
    }
}
