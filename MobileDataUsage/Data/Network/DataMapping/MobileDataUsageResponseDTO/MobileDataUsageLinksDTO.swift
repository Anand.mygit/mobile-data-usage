//
//  MobileDataUsageResponseDTO+Mapping.swift
//  mvvm-clean-architecture
//
//  Created by Anand, Chetan on 12/4/20.
//  Copyright © 2020 Anand, Chetan. All rights reserved.
//

import Foundation
struct MobileDataUsageLinksDTO: Codable {
    let start: String?
    let next: String?

    enum CodingKeys: String, CodingKey {
        case start
        case next
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        start = try values.decodeIfPresent(String.self, forKey: .start)
        next = try values.decodeIfPresent(String.self, forKey: .next)
    }
}
