//
//  MessageRequestDTO+Mapping.swift
//  mvvm-clean-architecture
//
//  Created by Anand, Chetan on 12/4/20.
//  Copyright © 2020 Anand, Chetan. All rights reserved.
//
import Foundation

struct MessageRequestDTO: Encodable {
    let id: String
}
