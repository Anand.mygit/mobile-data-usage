//
//  MobileDataUsageRequestDTO+Mapping.swift
//  mvvm-clean-architecture
//
//  Created by Anand, Chetan on 12/4/20.
//  Copyright © 2020 Anand, Chetan. All rights reserved.
//

import Foundation

struct MobileDataUsageRequestDTO: Encodable {
    let limit: Int
    let resourceId: String

    enum CodingKeys: String, CodingKey {
        case limit
        case resourceId = "resource_id"
    }
}
