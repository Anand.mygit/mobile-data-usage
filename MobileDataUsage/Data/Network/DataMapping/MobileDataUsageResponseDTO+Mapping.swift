//
//  MobileDataUsageResponseDTO+Mapping.swift
//  mvvm-clean-architecture
//
//  Created by Anand, Chetan on 12/4/20.
//  Copyright © 2020 Anand, Chetan. All rights reserved.
//
import Foundation

// MARK: - Data Transfer Object

struct MobileDataUsageResponseDTO: Decodable {
    let help: String?
    let success: Bool?
    let result: MobileDataUsageResultDTO?

    enum CodingKeys: String, CodingKey {
        case help
        case success
        case result
    }
}

// MARK: - Mappings to Domain

extension MobileDataUsageResponseDTO {
    func mapToDomain() -> MobileDataUsagePage {
        return MobileDataUsagePage(limit: 5, total: 10, records: result?.records?.compactMap { $0.mapToDomain() } ?? [MobileDataUsageRecord]())
    }
}

// MARK: - Private
