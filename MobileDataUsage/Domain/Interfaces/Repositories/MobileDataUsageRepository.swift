//
//  MobileDataUsageRepositoryInterfaces.swift
//  mvvm-clean-architecture
//
//  Created by Anand, Chetan on 12/4/20.
//  Copyright © 2020 Anand, Chetan. All rights reserved.
//

import Foundation

protocol MobileDataUsageRepository {
    @discardableResult
    func fetchMobileDataUsageList(requestValue: DisplayMobileDataUsageUseCaseRequestValue, cached: @escaping (MobileDataUsagePage) -> Void,
                                  completion: @escaping (Result<MobileDataUsagePage, Error>) -> Void) -> Cancellable?
}
