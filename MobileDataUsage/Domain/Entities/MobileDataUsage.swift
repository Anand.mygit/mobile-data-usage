//
//  MobileDataUsage.swift
//  mvvm-clean-architecture
//
//  Created by Anand, Chetan on 12/4/20.
//  Copyright © 2020 Anand, Chetan. All rights reserved.
//

import Foundation

typealias MobileDataUsageId = String

struct MobileDataUsageRecord: Equatable, Identifiable {
    let id: MobileDataUsageId
    var volume: Double?
    var quarter: String?
}

struct MobileDataUsagePage: Equatable {
    let limit: Int
    let total: Int
    let records: [MobileDataUsageRecord]
}

struct MobileDataUsageYearlyRecord: Equatable {
    var year: String?
    var quaterValues = [String: Double]()
}
