//
//  DisplayMobileDataUsageUseCase.swift
//  mvvm-clean-architecture
//
//  Created by Anand, Chetan on 12/4/20.
//  Copyright © 2020 Anand, Chetan. All rights reserved.
//

import Foundation

protocol DisplayMobileDataUsageUseCase {
    func execute(requestValue: DisplayMobileDataUsageUseCaseRequestValue,
                 cached: @escaping (MobileDataUsagePage) -> Void,
                 completion: @escaping (Result<MobileDataUsagePage, Error>) -> Void) -> Cancellable?
}

final class DefaultDisplayMobileDataUsageUseCase: DisplayMobileDataUsageUseCase {
    private let mobileDataUsagesRepository: MobileDataUsageRepository

    init(MobileDataUsageRepository: MobileDataUsageRepository) {
        mobileDataUsagesRepository = MobileDataUsageRepository
    }

    func execute(requestValue: DisplayMobileDataUsageUseCaseRequestValue,
                 cached: @escaping (MobileDataUsagePage) -> Void,
                 completion: @escaping (Result<MobileDataUsagePage, Error>) -> Void) -> Cancellable? {
        return mobileDataUsagesRepository.fetchMobileDataUsageList(requestValue: requestValue, cached: cached,
                                                                   completion: { result in

                                                                       if case .success = result {}

                                                                       completion(result)
        })
    }
}

struct DisplayMobileDataUsageUseCaseRequestValue {
    let limit: Int = 100
    let resourceId: String = "a807b7ab-6cad-4aa6-87d0-e283a7353a0f"
}
