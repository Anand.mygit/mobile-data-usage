//
//  AccessibilityIdentifier.swift
//  mvvm-clean-architecture
//
//  Created by Anand, Chetan on 12/4/20.
//  Copyright © 2020 Anand, Chetan. All rights reserved.
//

import Foundation

public struct AccessibilityIdentifier {
    static let MobileDataUsageDetailsView = "AccessibilityIdentifierMobileDataUsageDetailsView"
    static let displayField = "AccessibilityIdentifierDisplayMobileDataUsage"
}
