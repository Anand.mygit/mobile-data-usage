//
//  Alertable.swift
//  mvvm-clean-architecture
//
//  Created by Anand, Chetan on 12/4/20.
//  Copyright © 2020 Anand, Chetan. All rights reserved.
//

import UIKit

public protocol Alertable {}
public extension Alertable where Self: UIViewController {
    func showAlert(title: String = "", message: String, preferredStyle _: UIAlertController.Style = .alert, completion: (() -> Void)? = nil) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        present(alert, animated: true, completion: completion)
    }
}
