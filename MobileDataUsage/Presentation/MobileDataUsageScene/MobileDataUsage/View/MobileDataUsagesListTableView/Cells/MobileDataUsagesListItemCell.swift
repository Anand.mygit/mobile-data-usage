//
//  MobileDataUsagesListItemCellTableViewCell.swift
//  MobileDataUsage
//
//  Created by Anand, Chetan on 15/4/20.
//  Copyright © 2020 sphtech. All rights reserved.
//

import UIKit

final class MobileDataUsagesListItemCell: UITableViewCell {
    static let reuseIdentifier = String(describing: MobileDataUsagesListItemCell.self)

    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var dateLabel: UILabel!

    @IBOutlet var button: UIButton!

    var viewModel: MobileDataUsageItemViewModel? {
        didSet {
            if let viewModel = viewModel {
                fill(with: viewModel)
            }
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func fill(with viewModel: MobileDataUsageItemViewModel) {
        titleLabel.text = viewModel.volumeOfMobileData + " PB"
        dateLabel.text = viewModel.year
        button.isHidden = !viewModel.isDecreaseInQuaterlyUsages
        button.setTitle(nil, for: .normal)
        button.setImage(UIImage(named: "arrow.down.circle.fill"), for: .normal)
        button.tintColor = .red
        button.addTarget(self, action: #selector(buttonTapped), for: .touchUpInside)
    }

    @objc func buttonTapped() {
        if let topVC = UIApplication.getTopViewController() {
            let alert = UIAlertController(title: "", message: "There is a decrease in volume data for any quarter in this year", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            topVC.present(alert, animated: true, completion: nil)
        }
    }
}
