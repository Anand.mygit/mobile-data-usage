//
//  MobileDataUsagesListTableView.swift
//  MobileDataUsage
//
//  Created by Anand, Chetan on 15/4/20.
//  Copyright © 2020 sphtech. All rights reserved.
//

import Foundation
import UIKit

class MobileDataUsagesListView: UIView {
    var viewModel: MobileDataUsageViewModel!
    var items: [MobileDataUsageItemViewModel] = [] {
        didSet { reload() }
    }

    private lazy var tableView: UITableView = {
        let tableView = UITableView()
        return tableView
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        initViews()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initViews()
    }

    private func initViews() {
        // all the layout code from above
        backgroundColor = UIColor.white
        setupTableView()
    }

    private func bind(to viewModel: MobileDataUsageViewModel) {
        viewModel.loadingType.observe(on: self) { [weak self] in self?.update(isLoadingNextPage: $0 == .nextPage) }
    }

    private func setupTableView() {
        addSubview(tableView)
        tableView.backgroundView = UIView()
        tableView.delegate = self
        tableView.dataSource = self
        let cellNib = UINib(nibName: "MobileDataUsagesListItemCell", bundle: nil)
        tableView.register(cellNib, forCellReuseIdentifier: "MobileDataUsagesListItemCell")

        let footerView = UIView()
        footerView.backgroundColor = UIColor.white
        tableView.tableFooterView = footerView
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        tableView.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        tableView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        tableView.setNeedsLayout()
    }

    func reload() {
        tableView.reloadData()
    }

    func update(isLoadingNextPage: Bool) {
        if isLoadingNextPage {
        } else {
            tableView.tableFooterView = nil
        }
    }
}

extension MobileDataUsagesListView: UITableViewDelegate {
    func tableView(_: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewModel.didSelect(item: items[indexPath.row])
    }
}

extension MobileDataUsagesListView: UITableViewDataSource {
    func tableView(_: UITableView, numberOfRowsInSection _: Int) -> Int {
        return items.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: MobileDataUsagesListItemCell.reuseIdentifier, for: indexPath) as? MobileDataUsagesListItemCell else {
            fatalError("Cannot dequeue reusable cell \(MobileDataUsagesListItemCell.self) with reuseIdentifier: \(MobileDataUsagesListItemCell.reuseIdentifier)")
        }
        cell.viewModel = items[indexPath.row]

        if indexPath.row == items.count - 1 {}

        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt _: IndexPath) -> CGFloat {
        return viewModel.isEmpty ? tableView.frame.height : 70
    }
}
