//
//  MobileDataUsageListViewController.swift
//  mvvm-clean-architecture
//
//  Created by Anand, Chetan on 12/4/20.
//  Copyright © 2020 Anand, Chetan. All rights reserved.
//

import UIKit

final class MobileDataUsageViewController: UIViewController, StoryboardInstantiable, Alertable {
    @IBOutlet private var contentView: UIView!
    @IBOutlet private var emptyDataLabel: UILabel!
    @IBOutlet var mobileDataUsagesListView: MobileDataUsagesListView!

    private var viewModel: MobileDataUsageViewModel!

    static func create(with viewModel: MobileDataUsageViewModel) -> MobileDataUsageViewController {
        let view = MobileDataUsageViewController.instantiateViewController()
        view.viewModel = viewModel
        return view
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        title = viewModel.screenTitle
        emptyDataLabel.text = viewModel.emptyDataTitle
        setupRefreshButton()

        bind(to: viewModel)
        setupViews()
        viewModel.viewDidLoad()
    }

    private func bind(to viewModel: MobileDataUsageViewModel) {
        viewModel.items.observe(on: self) { [weak self] in
            self?.mobileDataUsagesListView?.items = $0
        }

        viewModel.messageOnSelect.observe(on: self) { [weak self] in self?.showMessage($0) }
        viewModel.error.observe(on: self) { [weak self] in self?.showError($0) }
        viewModel.loadingType.observe(on: self) { [weak self] _ in self?.updateViewsVisibility() }
    }

    private func setupViews() {
        mobileDataUsagesListView.viewModel = viewModel
    }

    private func showError(_ error: String) {
        guard !error.isEmpty else { return }
        showAlert(title: viewModel.errorTitle, message: error)
    }

    private func showMessage(_ value: String) {
        guard !value.isEmpty else { return }
        showAlert(title: "Message", message: value)
    }

    private func showMobileDataUsage(mobileDataUsagesListItemViewModels _: [MobileDataUsageItemViewModel]) {}

    private func updateViewsVisibility() {
        emptyDataLabel.isHidden = true
        LoadingView.hide()

        switch viewModel.loadingType.value {
        case .fullScreen: LoadingView.show()
        case .nextPage: break
        case .none:
            break
        }
    }

    @objc func reloadButtonClicked() {
        viewModel.didRefresh()
    }
}

extension MobileDataUsageViewController {
    private func setupRefreshButton() {
        let add = UIBarButtonItem(barButtonSystemItem: .refresh, target: self, action: #selector(reloadButtonClicked))
        navigationItem.rightBarButtonItem = add
    }
}
