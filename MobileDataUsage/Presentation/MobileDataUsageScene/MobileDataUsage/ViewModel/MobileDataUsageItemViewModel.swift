//
//  MobileDataUsageListItemViewModel.swift
//  mvvm-clean-architecture
//
//  Created by Anand, Chetan on 12/4/20.
//  Copyright © 2020 Anand, Chetan. All rights reserved.
//

import Foundation

struct MobileDataUsageItemViewModel: Equatable {
    let volumeOfMobileData: String
    let year: String
    var isDecreaseInQuaterlyUsages: Bool = false
}

extension MobileDataUsageItemViewModel {
    init(mobileDataUsage: MobileDataUsageYearlyRecord) {
        let volume = mobileDataUsage.quaterValues.values.reduce(0, +)
        let roundedVolume = round(1000 * volume) / 1000
        volumeOfMobileData = String(describing: roundedVolume)

        year = mobileDataUsage.year ?? ""

        var lastValue = 0.0
        let sortedQuaterValues = mobileDataUsage.quaterValues.sorted { $0.key < $1.key }

        for (_, value) in sortedQuaterValues {
            if lastValue > value {
                isDecreaseInQuaterlyUsages = true
            }
            lastValue = value
        }
    }
}
