//
//  MobileDataUsageListViewModel.swift
//  mvvm-clean-architecture
//
//  Created by Anand, Chetan on 12/4/20.
//  Copyright © 2020 Anand, Chetan. All rights reserved.
//

import Foundation

struct MobileDataUsageViewModelClosures {
    let showMobileDataUsageDetails: (String) -> Void
}

enum MobileDataUsageViewModelLoading {
    case fullScreen
    case nextPage
}

protocol MobileDataUsageListViewModelInput {
    func viewDidLoad()
    func didRefresh()
    func didSelect(item: MobileDataUsageItemViewModel)
}

protocol MobileDataUsageViewModelOutput {
    var items: Observable<[MobileDataUsageItemViewModel]> { get }
    var loadingType: Observable<MobileDataUsageViewModelLoading?> { get }
    var messageOnSelect: Observable<String> { get }
    var error: Observable<String> { get }
    var isEmpty: Bool { get }
    var screenTitle: String { get }
    var emptyDataTitle: String { get }
    var errorTitle: String { get }
}

protocol MobileDataUsageViewModel: MobileDataUsageListViewModelInput, MobileDataUsageViewModelOutput {}

final class DefaultMobileDataUsageViewModel: MobileDataUsageViewModel {
    private let displayMobileDataUsageUseCase: DisplayMobileDataUsageUseCase
    private let closures: MobileDataUsageViewModelClosures?

    private var mobileDataUsagesLoadTask: Cancellable? { willSet { mobileDataUsagesLoadTask?.cancel() } }
    private var yearlyItems = [MobileDataUsageYearlyRecord]()

    // MARK: - OUTPUT

    let items: Observable<[MobileDataUsageItemViewModel]> = Observable([])
    let loadingType: Observable<MobileDataUsageViewModelLoading?> = Observable(.none)
    let messageOnSelect: Observable<String> = Observable("")
    let error: Observable<String> = Observable("")
    var isEmpty: Bool { return items.value.isEmpty }
    let screenTitle = NSLocalizedString("Mobile Data Usage", comment: "")
    let emptyDataTitle = NSLocalizedString("No mobileDataUsages", comment: "")
    let errorTitle = NSLocalizedString("Error", comment: "")
    let displayBarPlaceholder = NSLocalizedString("Display MobileDataUsage", comment: "")

    init(displayMobileDataUsageUseCase: DisplayMobileDataUsageUseCase,
         closures: MobileDataUsageViewModelClosures? = nil) {
        self.displayMobileDataUsageUseCase = displayMobileDataUsageUseCase
        self.closures = closures
    }

    private func reloadMobileDataUsage(_ mobileDataUsages: [MobileDataUsageYearlyRecord]) {
        let sortedMobileDataUsage = mobileDataUsages.sorted(by: { (first, second) -> Bool in
            (first.year ?? "") < (second.year ?? "")
        })

        items.value = sortedMobileDataUsage.compactMap { MobileDataUsageItemViewModel(mobileDataUsage: $0) }
    }

    private func resetPages() {
        items.value.removeAll()
    }

    private func load(loadingType: MobileDataUsageViewModelLoading) {
        self.loadingType.value = loadingType

        mobileDataUsagesLoadTask = displayMobileDataUsageUseCase.execute(
            requestValue: .init(),
            cached: { page in
                self.computeMobileDataUsedYearly(items: page.records)
                self.reloadMobileDataUsage(self.yearlyItems)
            },
            completion: { result in
                switch result {
                case let .success(page):
                    self.computeMobileDataUsedYearly(items: page.records)
                    self.reloadMobileDataUsage(self.yearlyItems)
                case let .failure(error):
                    self.handle(error: error)
                }
                self.loadingType.value = .none
            }
        )
    }

    private func handle(error: Error) {
        self.error.value = error.isInternetConnectionError ?
            NSLocalizedString("No internet connection", comment: "") :
            NSLocalizedString("Failed loading MobileDataUsage", comment: "")
    }

    private func update() {
        resetPages()
        load(loadingType: .fullScreen)
    }
}

// MARK: - INPUT. View event methods

extension DefaultMobileDataUsageViewModel {
    func viewDidLoad() {
        update()
    }

    func didRefresh() {
        update()
    }

    func didSelect(item _: MobileDataUsageItemViewModel) {}
}

extension DefaultMobileDataUsageViewModel {
    func computeMobileDataUsedYearly(items: [MobileDataUsageRecord]) {
        yearlyItems.removeAll()
        for item in items {
            if let yearStringValue = item.quarter?.components(separatedBy: "-").first, let quarterStringValue = item.quarter?.components(separatedBy: "-").last {
                var existingitem = yearlyItems.filter { $0.year == yearStringValue }.first
                if existingitem != nil {
                    if let index = yearlyItems.firstIndex(of: existingitem!) {
                        yearlyItems.remove(at: index)
                        existingitem?.quaterValues.updateValue(item.volume ?? 0.0, forKey: quarterStringValue)
                        yearlyItems.append(existingitem!)
                    }

                } else {
                    var yearlyItem = MobileDataUsageYearlyRecord(year: yearStringValue)
                    yearlyItem.quaterValues.updateValue(item.volume ?? 0.0, forKey: quarterStringValue)
                    yearlyItems.append(yearlyItem)
                }
            }
        }
    }
}
