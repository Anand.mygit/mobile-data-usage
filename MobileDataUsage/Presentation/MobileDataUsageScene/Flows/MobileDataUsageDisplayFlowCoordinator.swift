//
//  MobileDataUsageDisplayFlowCoordinator.swift
//  mvvm-clean-architecture
//
//  Created by Anand, Chetan on 12/4/20.
//  Copyright © 2020 Anand, Chetan. All rights reserved.
//

import UIKit

protocol MobileDataUsageDisplayFlowCoordinatorDependencies {
    func makeMobileDataUsageViewController(closures: MobileDataUsageViewModelClosures) -> MobileDataUsageViewController
}

class MobileDataUsageDisplayFlowCoordinator {
    private let navigationController: UINavigationController
    private let dependencies: MobileDataUsageDisplayFlowCoordinatorDependencies

    private weak var mobileDataUsagesListVC: MobileDataUsageViewController?

    init(navigationController: UINavigationController,
         dependencies: MobileDataUsageDisplayFlowCoordinatorDependencies) {
        self.navigationController = navigationController
        self.dependencies = dependencies
    }

    func start() {
        // Note: here we keep strong reference with closures, this way this flow do not need to be strong referenced
        let closures = MobileDataUsageViewModelClosures(showMobileDataUsageDetails: showMobileDataUsageDetails)
        let vc = dependencies.makeMobileDataUsageViewController(closures: closures)

        navigationController.pushViewController(vc, animated: false)
        mobileDataUsagesListVC = vc
    }

    private func showMobileDataUsageDetails(MobileDataUsage _: String) {}
}
