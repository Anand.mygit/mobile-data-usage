//
//  AppFlowCoordinator.swift
//  mvvm-clean-architecture
//
//  Created by Anand, Chetan on 12/4/20.
//  Copyright © 2020 Anand, Chetan. All rights reserved.
//

import UIKit

class AppFlowCoordinator {
    var navigationController: UINavigationController
    private let appDIContainer: AppDIContainer

    init(navigationController: UINavigationController,
         appDIContainer: AppDIContainer) {
        self.navigationController = navigationController
        self.appDIContainer = appDIContainer
    }

    func start() {
        let mobileDataUsagesSceneDIContainer = appDIContainer.makeMobileDataUsageSceneDIContainer()
        let flow = mobileDataUsagesSceneDIContainer.makeMobileDataUsageDisplayFlowCoordinator(navigationController: navigationController)
        flow.start()
    }
}
