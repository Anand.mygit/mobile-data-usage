//
//  MobileDataUsageSceneDIContainer.swift
//  mvvm-clean-architecture
//
//  Created by Anand, Chetan on 12/4/20.
//  Copyright © 2020 Anand, Chetan. All rights reserved.
//

import SwiftUI
import UIKit

final class MobileDataUsageSceneDIContainer {
    struct Dependencies {
        let apiDataTransferService: DataTransferService
    }

    private let dependencies: Dependencies
    lazy var mobileDataUsageResponseCache: MobileDataUsageResponseStorage = CoreDataMobileDataUsagesResponseStorage()

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }

    // MARK: - Use Cases

    func makeDisplayMobileDataUsageUseCase() -> DisplayMobileDataUsageUseCase {
        return DefaultDisplayMobileDataUsageUseCase(MobileDataUsageRepository: makeMobileDataUsageRepository())
    }

    // MARK: - Repositories

    func makeMobileDataUsageRepository() -> MobileDataUsageRepository {
        return DefaultMobileDataUsageRepository(dataTransferService: dependencies.apiDataTransferService, cache: mobileDataUsageResponseCache)
    }

    // MARK: - MobileDataUsage List

    func makeMobileDataUsageViewController(closures: MobileDataUsageViewModelClosures) -> MobileDataUsageViewController {
        return MobileDataUsageViewController.create(with: makeMobileDataUsageViewModel(closures: closures))
    }

    func makeMobileDataUsageViewModel(closures: MobileDataUsageViewModelClosures) -> MobileDataUsageViewModel {
        return DefaultMobileDataUsageViewModel(displayMobileDataUsageUseCase: makeDisplayMobileDataUsageUseCase(),
                                               closures: closures)
    }

    // MARK: - Flow Coordinators

    func makeMobileDataUsageDisplayFlowCoordinator(navigationController: UINavigationController) -> MobileDataUsageDisplayFlowCoordinator {
        return MobileDataUsageDisplayFlowCoordinator(navigationController: navigationController,
                                                     dependencies: self)
    }
}

extension MobileDataUsageSceneDIContainer: MobileDataUsageDisplayFlowCoordinatorDependencies {}
