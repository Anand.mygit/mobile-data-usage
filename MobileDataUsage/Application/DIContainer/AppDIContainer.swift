//
//  DIContainer.swift
//  mvvm-clean-architecture
//
//  Created by Anand, Chetan on 12/4/20.
//  Copyright © 2020 Anand, Chetan. All rights reserved.
//

import Foundation
// Dependency Injection Containers of scenes

final class AppDIContainer {
    lazy var appConfiguration = AppConfiguration()

    // MARK: - Network

    lazy var apiDataTransferService: DataTransferService = {
        let config = ApiDataNetworkConfig(baseURL: URL(string: appConfiguration.apiBaseURL)!,
                                          queryParameters: [:])

        let apiDataNetwork = DefaultNetworkService(config: config)
        return DefaultDataTransferService(with: apiDataNetwork)
    }()

    // MARK: - Dependency Injection Containers of scenes

    func makeMobileDataUsageSceneDIContainer() -> MobileDataUsageSceneDIContainer {
        let dependencies = MobileDataUsageSceneDIContainer.Dependencies(apiDataTransferService: apiDataTransferService)
        return MobileDataUsageSceneDIContainer(dependencies: dependencies)
    }
}
