//
//  NetworkServiceMocks.swift
//  mvvm-clean-architecture
//
//  Created by Anand, Chetan on 12/4/20.
//  Copyright © 2020 Anand, Chetan. All rights reserved.
//

import Foundation
@testable import MobileDataUsage

class NetworkConfigurableMock: NetworkConfigurable {
    var baseURL: URL = URL(string: "http://localhost:9235")!
    var headers: [String: String] = [:]
    var queryParameters: [String: String] = [:]
}
