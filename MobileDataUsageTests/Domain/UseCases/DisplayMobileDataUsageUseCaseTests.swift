//
//  DisplayMobileDataUsageUseCaseTests.swift
//  mvvm-clean-architecture
//
//  Created by Anand, Chetan on 12/4/20.
//  Copyright © 2020 Anand, Chetan. All rights reserved.
//

@testable import MobileDataUsage
import XCTest

class DisplayMobileDataUsageUseCaseTests: XCTestCase {
    static let mockMobileDataUsage: [MobileDataUsageRecord] = {
        let page1 = MobileDataUsageRecord(id: "1", volume: 3.336984, quarter: "2010-Q1")
        let page2 = MobileDataUsageRecord(id: "2", volume: 3.466228, quarter: "2010-Q2")
        return [page1, page2]
    }()

    enum MobileDataUsageRepositorySuccessTestError: Error {
        case failedFetching
    }

    struct MobileDataUsageRepositoryMock: MobileDataUsageRepository {
        var result: Result<MobileDataUsagePage, Error>

        func fetchMobileDataUsageList(requestValue _: DisplayMobileDataUsageUseCaseRequestValue, cached _: @escaping (MobileDataUsagePage) -> Void, completion: @escaping (Result<MobileDataUsagePage, Error>) -> Void) -> Cancellable? {
            completion(result)
            return nil
        }
    }

    func test_DisplayMobileDataUsageUseCase_whenFailedFetchingMobileDataUsage_thenQueryCountIsZero() {
        // given
        let expectation = self.expectation(description: "Recent query should not be saved")
//        expectation.expectedFulfillmentCount = 2
        var mobileDataUsages: MobileDataUsagePage = MobileDataUsagePage(limit: 5, total: 10, records: [MobileDataUsageRecord]())
        let mobileDataUsagesRepositoryMock = MobileDataUsageRepositoryMock(result: .failure(MobileDataUsageRepositorySuccessTestError.failedFetching))

        let useCase = DefaultDisplayMobileDataUsageUseCase(MobileDataUsageRepository: mobileDataUsagesRepositoryMock)

        // when
        let requestValue = DisplayMobileDataUsageUseCaseRequestValue()
        _ = useCase.execute(requestValue: requestValue, cached: { _ in }) { result in
            switch result {
            case let .success(value):
                mobileDataUsages = value
            case let .failure(error):
                debugPrint(error)
            }
            expectation.fulfill()
        }
        // then

        waitForExpectations(timeout: 5, handler: nil)
        XCTAssertTrue(mobileDataUsages.records.count == 0)
    }
}
