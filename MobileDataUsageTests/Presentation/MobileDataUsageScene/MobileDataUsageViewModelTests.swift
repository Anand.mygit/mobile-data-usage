//
@testable import MobileDataUsage
//  MobileDataUsageListViewModelTests.swift
//  mvvm-clean-architecture
//
//  Created by Anand, Chetan on 12/4/20.
//  Copyright © 2020 Anand, Chetan. All rights reserved.
//
import XCTest

class MobileDataUsageListViewModelTests: XCTestCase {
    private enum DisplayMobileDataUsageUseCaseError: Error {
        case someError
    }

    static let mockMobileDataUsage: [MobileDataUsageRecord] = {
        let page1 = MobileDataUsageRecord(id: "1", volume: 3.336984, quarter: "2010-Q1")
        let page2 = MobileDataUsageRecord(id: "2", volume: 3.466228, quarter: "2010-Q2")
        return [page1, page2]
    }()

    class DisplayMobileDataUsageUseCaseMock: DisplayMobileDataUsageUseCase {
        var expectation: XCTestExpectation?
        var error: Error?

        func execute(requestValue _: DisplayMobileDataUsageUseCaseRequestValue,
                     cached _: @escaping (MobileDataUsagePage) -> Void,
                     completion: @escaping (Result<MobileDataUsagePage, Error>) -> Void) -> Cancellable? {
            if let error = error {
                completion(.failure(error))
            } else {
                let page = MobileDataUsagePage(limit: 5, total: 10, records: mockMobileDataUsage)
                completion(.success(page))
            }
            expectation?.fulfill()
            return nil
        }
    }

    func test_whenDisplayMobileDataUsageUseCaseRetrievesMobileDataUsageList_thenViewModelContainsMobileDataUsageList() {
        // given
        let displayMobileDataUsageUseCaseMock = DisplayMobileDataUsageUseCaseMock()

        displayMobileDataUsageUseCaseMock.expectation = expectation(description: "Displays mobileDataUsages")
        let viewModel = DefaultMobileDataUsageViewModel(displayMobileDataUsageUseCase: displayMobileDataUsageUseCaseMock)
        // when
        viewModel.didRefresh()

        // then
        waitForExpectations(timeout: 5, handler: nil)
        XCTAssertTrue(viewModel.items.value.count > 0)
    }
}
