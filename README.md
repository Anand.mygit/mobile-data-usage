# MobileDataUsage (XCode project)

This is a project done as per requirement given in : https://docs.google.com/forms/d/e/1FAIpQLSdl1Pxlrplt0m-SeSbkiY5XjuUACxf3YLw7r0pDaoOE3-ztsQ/viewform.

## Architecture (Clean architecture with MVVM)
Used Clean architecture with MVVM to 
- fix massive View Controller
- make it more testable
- build easy-to-maintain systems
- demonstrate Dependency injection (The dependency rule: inner layers should not depend on outer layers) and to
- de-couple business logic and application logic from the presentation layer


## Xcode version used 
```
Version 11.1 (11A1027)
```

## Deployment target
```
iOS 13.1
```

## API Base URL 
```
https://data.gov.sg/api/action
```
This can be changed in build settings by updating user defined key : API_BASE_URL

## Testing (Unit + UI)
```
Current code coverage : 82.4%
```
