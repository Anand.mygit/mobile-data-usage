//
//  MobileDataUsageUITests.swift
//  MobileDataUsageUITests
//
//  Created by Anand, Chetan on 14/4/20.
//  Copyright © 2020 sphtech. All rights reserved.
//

import XCTest

class MobileDataUsageUITests: XCTestCase {
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        XCUIApplication().launch()
        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }

    func test_ListShows_whenTapOnClickableImage_thenAlertMessageShows() {
        let app = XCUIApplication()
        let elementsQuery = app.tables.cells.buttons.firstMatch

        _ = elementsQuery.waitForExistence(timeout: 20)
        elementsQuery.tap()

        let alertButton = app.alerts.scrollViews.otherElements.buttons["OK"]
        _ = alertButton.waitForExistence(timeout: 1)
        alertButton.tap()
    }

    func testLaunchPerformance() {
        if #available(macOS 10.15, iOS 13.0, tvOS 13.0, *) {
            // This measures how long it takes to launch your application.
            measure(metrics: [XCTOSSignpostMetric.applicationLaunch]) {
                XCUIApplication().launch()
            }
        }
    }
}
